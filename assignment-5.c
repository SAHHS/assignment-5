#include<stdio.h>

#define PerformanceCost 500
#define PerAttendeeCost 3

int Attendees(int price);
int Income(int price);
int Cost(int price);
int Profit(int price);

int main()
{
	for(int price=5;price<50; price+=5)
	{
		printf("Ticket Price = Rs.%d \t\t Profit = Rs.%d\n",price,Profit(price));
		printf("...........................................\n\n");
    }
}

int Profit(int price){
	return Income(price)-Cost(price);
}

int	Attendees(int price){
	if(price>15)
    {
        return 120-(((price-15)/5)*20);
    }
    else{
        return 120+(((15-price)/5)*20);
    }
}
int Income(int price)
{
    return Attendees(price)*price;

}

int Cost(int price){
	return Attendees(price)*PerAttendeeCost+PerformanceCost;
}





